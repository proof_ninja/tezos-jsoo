open Js_of_ocaml
open Js_of_ocaml_lwt

include Jsx
include Misc

module Infix = struct
  include Lwt.Infix

  let (>>?) at f =
    match at with
    | Error _ as e -> e
    | Ok v -> f v

  let (>|?) at f =
    match at with
    | Error _ as e -> e
    | Ok v -> Ok (f v)

  let (>>=?) at f =
    at >>= function
    | Error _ as e -> Lwt.return e
    | Ok x -> f x

  let (>|=?) at f =
    at >|= function
    | Error _ as e -> e
    | Ok x -> Ok (f x)
end

open Infix

module Node = struct
  type t = { base_url : string }
  let make base_url = { base_url }
end

type address = Address of string

type mutez = Mutez of Z.t

module Mutez = struct
  type t = mutez
  let to_string (Mutez z) = "utez " ^ Z.to_string z
end

module RPC = struct
  module Path = struct
    let head = "/chains/main/blocks/head"
    let contract ?(block=head) c = block ^/ "context/contracts" ^/ c
  end

  let url node path =
    from_Some @@ Url.url_of_string @@ node.Node.base_url ^ path

  type Error.t += HTTP of XmlHttpRequest.http_frame
               | JSON_parse of exn

  let () =
    let open Format in
    Error.register_printer @@ fun ppf -> function
    | HTTP http_frame -> fprintf ppf "HTTP %d" http_frame.code; true
    | JSON_parse exn -> fprintf ppf "JSON parse error: %s" (Printexc.to_string exn); true
    | _ -> false

  let get node path =
    let url = url node path in
    (* The node must be CORS enabled.
       See https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
    *)
    XmlHttpRequest.perform url
    >|= fun http_frame ->
    match http_frame.code with
    | 200 ->
        begin try
            Ok (json##parse (Js.string http_frame.content))
          with exn -> Error (JSON_parse exn)
        end
    | _ -> Error (HTTP http_frame)

  (* 404 is not an error but None *)
  let get_opt node path =
    get node path >|= function
    | Error (HTTP http_frame) when http_frame.code = 404 -> Ok None
    | Ok x -> Ok (Some x)
    | Error _ as e -> e

  let contract n node ?block (Address c) =
    get node (Path.contract ?block c ^/ n)

  let _contract_opt n node ?block (Address c) =
    get_opt node (Path.contract ?block c ^/ n)

  type 'a contract_query = Node.t -> ?block:string -> address -> ('a, Error.t) result Lwt.t

  let balance node ?block a =
    contract "balance" node ?block a >|=?
    fun (s : Js.js_string Js.t) -> Mutez (Z.of_string (Js.to_string s))

  let counter node ?block a =
    contract "counter" node ?block a
    >|=? fun (s : Js.js_string Js.t) -> Z.of_string (Js.to_string s)

  let manager_key node ?block a =
    contract "manager_key" node ?block a
    >|=? fun (s : Js.js_string Js.t Js.Opt.t) ->
    Option.map Js.to_string @@ Js.Opt.to_option s

  class type contract =
    let open Js in
    object
      method balance : js_string t readonly_prop
      method delegate : js_string t optdef readonly_prop
      method script : < > t optdef readonly_prop
      method counter : js_string t optdef readonly_prop
    end

  let contract' node ?block (Address c) =
    (get node (Path.contract ?block c) : (contract Js.t, Error.t) result Lwt.t)
end

module Base58check = Base58check
module Sodium = Sodium
module Uint8array = Uint8array

module Ed25519 = struct
  module Seed = struct
    (* 32 byte *)
    let decode s = Base58check.decode s 4
    let encode b = Base58check.encode b (Js.string "0d0f3a07")
  end

  module Secret_key = struct
    (* 64 byte *)
    let decode s = Base58check.decode s 4
    let encode b = Base58check.encode b (Js.string "2bf64e07")
  end

  module Public_key = struct
    (* 32 byte *)
    let decode s = Base58check.decode s 4
    let encode b = Base58check.encode b (Js.string "0d0f25d9")
  end
end

module Beacon = Beacon
