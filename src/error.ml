open Js_of_ocaml
open Jsx

type t = ..

let printers = ref ([] : (Format.formatter -> t -> bool) list)

let register_printer p = printers := p :: !printers

let pp ppf e =
  let open Format in
  let rec loop = function
    | [] -> fprintf ppf "unprintable error (%s)" @@ Js.to_string @@ json##stringify e
    | p::ps ->
        if p ppf e then ()
        else loop ps
  in
  loop !printers
