open Js_of_ocaml
open Js_of_ocaml_lwt

include module type of Jsx
include module type of Misc

module Infix : sig
  val ( >>= ) : 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t
  val ( >|= ) : 'a Lwt.t -> ('a -> 'b) -> 'b Lwt.t
  val ( <&> ) : unit Lwt.t -> unit Lwt.t -> unit Lwt.t
  val ( <?> ) : 'a Lwt.t -> 'a Lwt.t -> 'a Lwt.t
  val ( =<< ) : ('a -> 'b Lwt.t) -> 'a Lwt.t -> 'b Lwt.t
  val ( =|< ) : ('a -> 'b) -> 'a Lwt.t -> 'b Lwt.t
  module Let_syntax = Lwt.Infix.Let_syntax

  val (>>?) : ('a, 'b) result -> ('a -> ('c, 'b) result) -> ('c, 'b) result
  val (>|?) : ('a, 'b) result -> ('a -> 'c) -> ('c, 'b) result
  val ( >>=? ) :
    ('a, 'b) result Lwt.t ->
    ('a -> ('c, 'b) result Lwt.t) -> ('c, 'b) result Lwt.t
  val ( >|=? ) :
    ('a, 'b) result Lwt.t -> ('a -> 'c) -> ('c, 'b) result Lwt.t
end

module Node : sig type
  t = { base_url : string; }
  val make : string -> t
end

type address = Address of string

type mutez = Mutez of Z.t

module Mutez : sig
  type t = mutez
  val to_string : t -> string
end

module RPC : sig

  type Error.t += HTTP of XmlHttpRequest.http_frame
               | JSON_parse of exn

  type 'a contract_query = Node.t -> ?block:string -> address -> ('a, Error.t) result Lwt.t

  val get : Node.t -> string -> ('a Js.t, Error.t) result Lwt.t

  val balance : mutez contract_query
  val counter : Z.t contract_query
  val manager_key : string option contract_query

  class type contract =
    let open Js in
    object
      method balance : js_string t readonly_prop
      method delegate : js_string t optdef readonly_prop
      method script : < > t optdef readonly_prop
      method counter : js_string t optdef readonly_prop
    end

  val contract' : Node.t -> ?block:string -> address -> (contract Js.t, Error.t) result Lwt.t
end

module Base58check = Base58check
module Sodium = Sodium
module Uint8array = Uint8array

module Ed25519 : sig
  module Seed : sig
    (* 54 chars of "edsk..." in .tezos-client/secret_keys is actually an unencrypted
       Ed25519 32 byte seed. *)
    val decode : Js.js_string Js.t -> Uint8array.t Js.t
    val encode : Uint8array.t Js.t -> Js.js_string Js.t
  end

  module Secret_key : sig
    (* 96 chars of "edsk..." for Ed25519 64 byte secret key *)
    val decode : Js.js_string Js.t -> Uint8array.t Js.t
    val encode : Uint8array.t Js.t -> Js.js_string Js.t
  end

  module Public_key : sig
    (* 54 chars of "edpk..." in .tezos-client/public_keys for Ed25519 32 byte public key *)
    val decode : Js.js_string Js.t -> Uint8array.t Js.t
    val encode : Uint8array.t Js.t -> Js.js_string Js.t
  end
end

module Beacon = Beacon
