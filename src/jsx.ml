open Js_of_ocaml

module Html = Dom_html

let document = Html.window##.document

let alert s = Html.window##alert (Js.string s)

exception No_element_id of string

let getById n =
  match Html.getElementById n with
  | exception Not_found -> raise (No_element_id n)
  | x -> x

let (!$) = Js.string
let (?$) = Js.to_string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

let json : < parse : 'a . Js.js_string Js.t -> 'a Js.meth;
             stringify : 'a. 'a -> Js.js_string Js.t Js.meth > Js.t = Js.Unsafe.pure_js_expr "JSON"

let append_js_string parent s = Dom.appendChild parent @@ document##createTextNode s
let append_string parent s = append_js_string parent @@ Js.string s
let appendf parent fmt = Format.kasprintf (append_string parent) fmt
let appendp parent = Dom.appendChild parent @@ document##createElement (Js.string "p")
let append_json parent j = append_js_string parent (json##stringify j)
