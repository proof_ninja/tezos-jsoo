open Js_of_ocaml
open Jsx
open Js
(* open Js_of_ocaml_lwt *)

type client

(* beacon-sdk/src/types/beacon/BeaconMessageType.ts *)
module BeaconMessageType = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _PermissionRequest   = make "permission_request"
  let _SignPayloadRequest  = make "sign_payload_request"
  let _OperationRequest    = make "operation_request"
  let _BroadcastRequest    = make "broadcast_request"
  let _PermissionResponse  = make"permission_response"
  let _SignPayloadResponse = make "sign_payload_response"
  let _OperationResponse   = make "operation_response"
  let _BroadcastResponse   = make "broadcast_response"
  let _Disconnect          = make "disconnect"
  let _Error               = make "error"
end

(* beacon-sdk/src/types/beacon/BeaconBaseMessage.ts *)
class type beaconBaseMessage = object
  method type_ : BeaconMessageType.t readonly_prop
  method version : js_string t readonly_prop

  (* ID of the message. The same ID is used in the request and response *)
  method id : js_string t readonly_prop

  (* ID of the sender. This is used to identify the sender of the message *)
  method senderId : js_string t readonly_prop
end

(* beacon-sdk/src/types/beacon/PermissionScope.ts *)
module PermissionScope = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _SIGN = make "sign"
  let _OPERATION_REQUEST = make "operation_request"

  (* Allows the DApp to send encryption and decryption requests *)
  let _ENCRYPT = make "encrypt" (* not defined in tzip-10 *)
  let _THRESHOLD = make "threshold"
end

(* beacon-sdk/src/types/beacon/NetworkType.ts *)
module NetworkType = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _MAINNET = make "mainnet"
  let _HANGZHOUNET = make "hangzhounet"
end

(* beacon-sdk/src/types/beacon/Network.ts *)
class type network = object
  method type_ : NetworkType.t prop
  method name : js_string t optdef prop
  method rpcUrl : js_string t optdef prop
end

(* beacon-sdk/src/types/beacon/AppMetaData.ts *)
class type appMetadata = object
  method senderId: js_string t readonly_prop
  method name: js_string t readonly_prop

  (* tzip-10 says: TODO: Should this be a URL or base64 image? *)
  method icon : js_string t optdef readonly_prop
end

(* beacon-sdk/src/types/RequestPermissionInput.ts *)
class type requestPermissionInput = object
  method network : network optdef readonly_prop
  method scopes : PermissionScope.t js_array optdef readonly_prop
end

(* beacon-sdk/src/types/beacon/Threshold.ts *)
class type threshold = object
  method amount : js_string t readonly_prop
  method timeframe : js_string t readonly_prop
end

(* beacon-sdk/src/types/beacon/messages/PermissionResponse.ts *)
class type permissionResponse = object
  inherit beaconBaseMessage

  (* type: MessageType.PermissionResponse; *)

  (* Public Key, because it can be used to verify signatures *)
  method publicKey: js_string t readonly_prop

  (* Network on which the permissions have been granted *)
  method network : network readonly_prop

  (* Permissions that have been granted for this specific address / account *)
  method scopes : PermissionScope.t js_array t readonly_prop

  method threashold : threshold optdef readonly_prop
end

(* beacon-sdk/src/types/BeaconErrorType.ts *)
module BeaconErrorType = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  (**
   * {@link BroadcastBeaconError}
   *
   * Will be returned if the user chooses that the transaction is broadcast but there is an error (eg. node not available).
   *
   * Returned by: Broadcast | Operation Request
  *)
  let _BROADCAST_ERROR = make "BROADCAST_ERROR"

  (**
   * {@link NetworkNotSupportedBeaconError}
   *
   * Will be returned if the selected network is not supported by the wallet / extension.
   *
   * Returned by: Permission
  *)
  let _NETWORK_NOT_SUPPORTED = make "NETWORK_NOT_SUPPORTED"

  (**
   * {@link NoAddressBeaconError}
   *
   * Will be returned if there is no address present for the protocol / network requested.
   *
   * Returned by: Permission
  *)
  let _NO_ADDRESS_ERROR = make "NO_ADDRESS_ERROR"

  (**
   * {@link NoPrivateKeyBeaconError}
   *
   * Will be returned if the private key matching the sourceAddress could not be found.
   *
   * Returned by: Sign
  *)
  let _NO_PRIVATE_KEY_FOUND_ERROR = make "NO_PRIVATE_KEY_FOUND_ERROR"

  (**
   * {@link NotGrantedBeaconError}
   *
   * Will be returned if the signature was blocked // (Not needed?) Permission: Will be returned if the permissions requested by the App were not granted.
   *
   * Returned by: Sign
  *)
  let _NOT_GRANTED_ERROR = make "NOT_GRANTED_ERROR"

  (**
   * {@link ParametersInvalidBeaconError}
   *
   * Will be returned if any of the parameters are invalid.
   *
   * Returned by: Operation Request
  *)
  let _PARAMETERS_INVALID_ERROR = make "PARAMETERS_INVALID_ERROR"

  (**
   * {@link TooManyOperationsBeaconError}
   *
   * Will be returned if too many operations were in the request and they were not able to fit into a single operation group.
   *
   * Returned by: Operation Request
  *)
  let _TOO_MANY_OPERATIONS = make "TOO_MANY_OPERATIONS"

  (**
   * {@link TransactionInvalidBeaconError}
   *
   * Will be returned if the transaction is not parsable or is rejected by the node.
   *
   * Returned by: Broadcast
   *)
  let _TRANSACTION_INVALID_ERROR = make "TRANSACTION_INVALID_ERROR"

  (**
   * {@link SignatureTypeNotSupportedBeaconError}
   *
   * Will be returned if the signing type is not supported.
   *
   * Returned by: Sign
   *)
  let _SIGNATURE_TYPE_NOT_SUPPORTED = make "SIGNATURE_TYPE_NOT_SUPPORTED"

  (*
  // TODO: ENCRYPTION
  // /**
  //  * {@link EncryptionTypeNotSupportedBeaconError}
  //  *
  //  * Will be returned if the encryption type is not supported.
  //  *
  //  * Returned by: Encrypt
  //  */
  // ENCRYPTION_TYPE_NOT_SUPPORTED = 'ENCRYPTION_TYPE_NOT_SUPPORTED',
  *)

  (**
   * {@link AbortedBeaconError}
   *
   * Will be returned if the request was aborted by the user or the wallet.
   *
   * Returned by: Permission | Operation Request | Sign Request | Broadcast
   *)
  let _ABORTED_ERROR = make "ABORTED_ERROR"

  (**
   * {@link UnknownBeaconError}
   *
   * Used as a wildcard if an unexpected error occured.
   *
   * Returned by: Permission | Operation Request | Sign Request | Broadcast
   *)
  let _UNKNOWN_ERROR = make "UNKNOWN_ERROR"
end

(* beacon-sdk/src/types/beacon/messages/ErrorResponse.ts *)
class type errorResponse = object
  inherit beaconBaseMessage

  (* type: MessageType.BeaconMessageType.Error; *)
  method errorType : BeaconErrorType.t readonly_prop
  method errorData : Js.Unsafe.any readonly_prop
end

(* beacon-sdk/src/clients/dapp-client/DAppClientOptions.ts *)
class type dAppClientOptions = object
  method name : js_string t readonly_prop
  method iconUrl : js_string t optdef readonly_prop
  method appUrl : js_string t optdef readonly_prop
(*
    storage?: Storage

    eventHandlers?: {
      [key in BeaconEvent]?: {
        handler: BeaconEventHandlerFunction<BeaconEventType[key]>
      }
    }

    disableDefaultEvents?: boolean
*)
  method matrixNodes : js_string t js_array optdef readonly_prop

(*
    blockExplorer?: BlockExplorer
*)

  method preferredNetwork : NetworkType.t t optdef readonly_prop

(*
    colorMode?: ColorMode
*)

  method disclaimerText : js_string t optdef readonly_prop
end

let new_DAppClient ~name ?iconUrl ?appUrl ?matrixNodes ?preferredNetwork ?disclaimerText () =
  let x : dAppClientOptions Js.t =
    object%js
      val name = name
      val iconUrl = Js.Optdef.option iconUrl
      val appUrl = Js.Optdef.option appUrl
      val matrixNodes = Js.Optdef.option matrixNodes
      val preferredNetwork = Js.Optdef.option preferredNetwork
      val disclaimerText = Js.Optdef.option disclaimerText
    end
  in
  Js.Unsafe.(new_obj (variable "beacon.DAppClient") [|inject x|])

let requestPermissions ?network ?scopes client : permissionResponse Js.t Promise.t =
  let x : requestPermissionInput Js.t =
    object%js
      val network = Js.Optdef.option network
      val scopes = Js.Optdef.option scopes
    end
  in
  Js.Unsafe.(meth_call client "requestPermissions" [| inject x |])

(* beacon-sdk/src/types/PermissionInfo.ts *)
class type permissionEntity = object
  method address : js_string t readonly_prop
  method network : network t readonly_prop
  method scopes : PermissionScope.t js_array t readonly_prop
  method threshold : threshold t optdef readonly_prop
end

(* beacon-sdk/src/types/Origin.ts *)
module Origin = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _WEBSITE = make "website"
  let _EXTENSION = make "extension"
  let _P2P = make "p2p"
end

(* beacon-sdk/src/types/AccountInfo.ts *)
class type accountInfo = object
  inherit permissionEntity
  method accountIdentifier : js_string t readonly_prop
  method senderId : js_string t readonly_prop
  method origin : < type_ : Origin.t readonly_prop; id : js_string t readonly_prop > t readonly_prop
  method publicKey : js_string t readonly_prop
  method connectedAt : number t readonly_prop
end

let getActiveAccount client : accountInfo Js.t Promise.t =
  Js.Unsafe.(meth_call client "getActiveAccount" [| |])

let setActiveAccount client (account : accountInfo Js.t) : unit Promise.t =
  Js.Unsafe.(meth_call client "setActiveAccount" [| inject account |])

let clearActiveAccount client : unit Promise.t =
  Js.Unsafe.(meth_call client "clearActiveAccount" [| |])

(**
 * Will remove the account from the local storage and set a new active account if necessary.
 *
 * @param accountIdentifier ID of the account
 *)
let removeActiveAccount client (accountIdentifier : Js.js_string Js.t) : unit Promise.t =
  Js.Unsafe.(meth_call client "removeAccount" [| inject accountIdentifier |])

let removeAllAccounts client : unit Promise.t =
  Js.Unsafe.(meth_call client "removeAllAccounts" [| |])

(**
 * Check if we have permissions to send the specific message type to the active account.
 * If no active account is set, only permission requests are allowed.
 *
 * @param type The type of the message
 *)
let checkPermissions client (type_: BeaconMessageType.t) : bool Promise.t =
  Js.Unsafe.(meth_call client "checkPermissions" [| inject type_ |])

(* beacon-sdk/src/types/beacon/SigningType.ts *)
module SigningType = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _RAW = make "raw"
  let _OPERATION = make "operation"
  let _MICHELINE = make "micheline"
end

(* beacon-sdk/src/types/RequestSignPayloadInput.ts *)
class type requestSignPayloadInput = object
  method signingType : SigningType.t optdef readonly_prop
  method payload : js_string t readonly_prop
  method sourceAddress : js_string t optdef readonly_prop
end

(* beacon-sdk/src/types/beacon/messages/SignPayloadResponse.ts *)
class type signPayloadResponse = object
  inherit beaconBaseMessage
  (* type: BeaconMessageType.SignPayloadResponse *)
  method signingType : SigningType.t readonly_prop
  method signature : js_string t readonly_prop
end

(* beacon-sdk/src/types/beacon/messages/BeaconResponseOutputMessage.ts *)
type signPayloadResponseOutput = signPayloadResponse

 (**
   * This method will send a "SignPayloadRequest" to the wallet. This method is meant to be used to sign
   * arbitrary data (eg. a string). It will return the signature in the format of "edsig..."
   *
   * @param input The message details we need to prepare the SignPayloadRequest message.
  *)
let requestSignPayload client (input : requestSignPayloadInput Js.t) : signPayloadResponseOutput Js.t Promise.t =
  Js.Unsafe.(meth_call client "requestSignPayload" [| inject input |])

(* beacon-sdk/src/types/tezos/OperationTypes.ts *)
module TezosOperationType = struct
  type t

  let make n : t = Obj.magic @@ !$ n

  let _ENDORSEMENT = make "endorsement"
  let _SEED_NONCE_REVELATION = make "seed_nonce_revelation"
  let _DOUBLE_ENDORSEMENT_EVIDENCE = make "double_endorsement_evidence"
  let _DOUBLE_BAKING_EVIDENCE = make "double_baking_evidence"
  let _ACTIVATE_ACCOUNT = make "activate_account"
  let _PROPOSALS = make "proposals"
  let _BALLOT = make "ballot"
  let _REVEAL = make "reveal"
  let _TRANSACTION = make "transaction"
  let _ORIGINATION = make "origination"
  let _DELEGATION = make "delegation"
end

(* beacon-sdk/src/types/tezos/TezosBaseOperation.ts *)
class type tezosBaseOperation = object
  method kind : TezosOperationType.t readonly_prop
end

type michelineMichelsonV1Expression

(* beacon-sdk/src/types/tezos/TezosTransactionParameters.ts *)
class type tezosTransactionParameters = object
  method entrypoint : js_string t readonly_prop
  method value : michelineMichelsonV1Expression t readonly_prop
end

(* beacon-sdk/src/types/tezos/operations/Transaction.ts *)
class type tezosTransactionOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.TRANSACTION *)
  method source : js_string t readonly_prop
  method fee : js_string t readonly_prop
  method counter: js_string t readonly_prop
  method gas_limit: js_string t readonly_prop
  method storage_limit: js_string t readonly_prop
  method amount: js_string t readonly_prop
  method destination: js_string t readonly_prop
  method parameters : tezosTransactionParameters t optdef readonly_prop
end

(* beacon-sdk/src/types/tezos/PartialTezosOperation.ts *)
class type partialTezosTransactionOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.TRANSACTION *)
  (* method source : js_string t readonly_prop
     method fee : js_string t readonly_prop
     method counter: js_string t readonly_prop
     method gas_limit: js_string t readonly_prop
     method storage_limit: js_string t readonly_prop
  *)
  method amount: js_string t readonly_prop
  method destination: js_string t readonly_prop
  method parameters : tezosTransactionParameters t optdef readonly_prop
end

(* beacon-sdk/src/types/tezos/operations/Delegation.ts *)
class type tezosDelegationOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.DELEGATION *)
  method source: js_string t readonly_prop
  method fee: js_string t readonly_prop
  method counter: js_string t readonly_prop
  method gas_limit: js_string t readonly_prop
  method storage_limit: js_string t readonly_prop
  method delegate : js_string t optdef readonly_prop
end

(* beacon-sdk/src/types/tezos/PartialTezosOperation.ts *)
class type partialTezosDelegationOperation = object
  inherit tezosBaseOperation
  (* kind: TezosOperationType.DELEGATION *)
  (* method source: js_string t readonly_prop
     method fee: js_string t readonly_prop
     method counter: js_string t readonly_prop
     method gas_limit: js_string t readonly_prop
     method storage_limit: js_string t readonly_prop
  *)
  method delegate : js_string t optdef readonly_prop
end

(* beacon-sdk/src/types/tezos/PartialTezosOperation.ts *)
type partialTezosOperation = tezosBaseOperation

let partialTezosOperation x = (x :> partialTezosOperation Js.t)

(*
            {
              kind: beacon.TezosOperationType.DELEGATION,
              delegate: 'tz1MJx9vhaNRSimcuXPK2rW4fLccQnDAnVKJ'
            }
              {
                kind: beacon.TezosOperationType.TRANSACTION,
                destination: activeAccount?.address ?? '',
                amount: '1'
              }

              {
                kind: beacon.TezosOperationType.TRANSACTION,
                amount: '0',
                destination: 'KT1RxKJyi48W3bZR8HErRiisXZQw19HwLGWj',
                parameters: {
                  entrypoint: 'toggleStatus',
                  value: {
                    prim: 'True'
                  }
                }
              }
*)

let partialTezosTransactionOperation ~mutez ~destination ?parameters () =
  let amount = (!$) @@ Int64.to_string mutez in
  let o : partialTezosTransactionOperation Js.t =
    object%js
      val kind = TezosOperationType._TRANSACTION
      val amount = amount
      val destination = destination
      val parameters = Js.Optdef.option parameters
    end
  in
  partialTezosOperation o

let partialTezosDelegationOperation ~delegate =
  let o : partialTezosDelegationOperation Js.t =
    object%js
      val kind = TezosOperationType._DELEGATION
      val delegate = Js.Optdef.option delegate
    end
  in
  partialTezosOperation o

(* beacon-sdk/src/types/RequestOperationInput.ts *)
class type requestOperationInput = object
  method operationDetails : partialTezosOperation Js.t js_array t readonly_prop
end

(* beacon-sdk/src/types/beacon/messages/OperationResponse.ts *)
class type operationResponse = object
  inherit beaconBaseMessage
  (* type: BeaconMessageType.OperationResponse *)
  method transactionHash : js_string t readonly_prop
end

(* beacon-sdk/src/types/beacon/messages/BeaconReponseOutputMessage.ts *)
type operationResponseOutput = operationResponse

(**
 * This method sends an OperationRequest to the wallet. This method should be used for all kinds of operations,
 * eg. transaction or delegation. Not all properties have to be provided. Data like "counter" and fees will be
 * fetched and calculated by the wallet (but they can still be provided if required).
 *
 * @param input The message details we need to prepare the OperationRequest message.
*)
let requestOperation client (input: requestOperationInput Js.t) : operationResponseOutput Js.t Promise.t =
  Js.Unsafe.(meth_call client "requestOperation" [| inject input |])

let requestOperation client (operationDetails : partialTezosOperation Js.t array) =
  requestOperation client @@ object%js val operationDetails = Js.array operationDetails end

    (*
    let requestOperationInput =
      let o = object%js
        val type_ = TezosOperationType._TRANSACTION

    (* beacon-sdk/src/types/beacon/BeaconBaseMessage.ts *)
    class type beaconBaseMessage =
      let open Js in
      object
      method type_ : BeaconMessageType.t readonly_prop
      method version : js_string t readonly_prop

      (* ID of the message. The same ID is used in the request and response *)
      method id : js_string t readonly_prop

      (* ID of the sender. This is used to identify the sender of the message *)
      method senderId : js_string t readonly_prop
    end

    *)
