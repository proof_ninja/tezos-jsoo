open Js_of_ocaml
open Js

let decode s prefixbytes =
  (Js.Unsafe.js_expr "b58c_decode" : js_string t -> int -> Uint8array.t t )
    s prefixbytes

let encode bin prefix =
  (Js.Unsafe.js_expr "b58c_encode" : Uint8array.t t -> js_string t -> js_string t)
    bin prefix
