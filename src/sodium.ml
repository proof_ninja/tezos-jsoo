open Js_of_ocaml
open Js

class type keypair = object
  method publicKey : Uint8array.t t readonly_prop
  method privateKey : Uint8array.t t readonly_prop
end

class type sodium = object
  method from_hex : js_string t -> Uint8array.t t meth
  method to_hex : Uint8array.t t -> js_string t meth

  method crypto_sign_seed_keypair_ : Uint8array.t t -> keypair t meth
  method crypto_generichash_ : int -> Uint8array.t t -> Uint8array.t t meth
  method crypto_sign_detached_ : mes:Uint8array.t t -> sk:Uint8array.t t -> Uint8array.t t meth
  method crypto_sign_verify_detached_ : sign:Uint8array.t t -> mes:Uint8array.t t -> pk:Uint8array.t t -> bool t meth
end

type t = sodium

(* sodium.js loading is async, therefore we need a special onload setting *)
class type onload =
  let open Js in
  object
    method onload : (Dom_html.window Js.t, sodium Js.t -> unit) Js.meth_callback prop
  end

class type window' =
  let open Js in
  object
    (* before being loaded, the user sets [sodium] an on-load callback *)
    method sodium_onload : onload Js.t prop

    (* once loaded, [sodium] is replaced by the sodium library object *)
    method sodium : sodium Js.t readonly_prop
  end

let window' : window' Js.t = Js.Unsafe.global

let with_sodium f =
  let g _this x = f x in
  let x : onload Js.t = Js.Unsafe.obj [| "onload", Js.Unsafe.(inject @@ meth_callback g) |] in
  window'##.sodium_onload := x
