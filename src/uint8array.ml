open Js_of_ocaml

type t

class type textEncoder = object
  method encode : Js.js_string Js.t -> t Js.t Js.meth
end

let textEncoder = Js.Unsafe.global##._TextEncoder

let encode s = (new%js textEncoder)##encode s
