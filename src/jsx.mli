open Js_of_ocaml

(** [alert] of Js *)
val alert : string -> unit

(* wrapper of [getElementById] *)
val getById : string -> Dom_html.element Js.t

(** Equivalent of [Js.string] *)
val (!$) : string -> Js.js_string Js.t

(** Equivalent of [Js.to_string] *)
val (?$) : Js.js_string Js.t -> string

module Console : sig
  (** console.log(_) *)
  val log : 'a -> unit

  (** console.log(_) with format interface *)
  val logf : ('a, Format.formatter, unit, unit) format4 -> 'a
end

(** JSON parser and stringify object *)
val json : < parse : 'a . Js.js_string Js.t -> 'a Js.meth;
             stringify : 'a. 'a -> Js.js_string Js.t Js.meth > Js.t

(** The document *)
val document : Dom_html.document Js.t

(** Append strings to the specified Dom element *)
val append_js_string : #Dom.node Js.t -> Js.js_string Js.t -> unit
val append_string : #Dom.node Js.t -> string -> unit
val append_json : #Dom.node Js.t -> < .. > Js.t -> unit
val appendf : #Dom.node Js.t -> ('a, Format.formatter, unit, unit) format4 -> 'a
(** Append <p> to the specified Dom element *)
val appendp : #Dom.node Js.t -> unit
