let (^/) = Filename.concat

let from_Some = function None -> invalid_arg "from_Some" | Some x -> x
