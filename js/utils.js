function b58c_decode(s, prefix_bytes) {
    let hex = base58check.decode(s, 'hex');
    hex = hex.prefix + hex.data;
    return sodium.from_hex( hex.substring( prefix_bytes * 2 ) );
}

function b58c_encode(bin, prefix_hex) {
    return base58check.encode(sodium.to_hex(bin), prefix= prefix_hex);
}

// beacon
// 
// function beacon_new_DAppClient(param) {
//     return new beacon.DAppClient(param);
// }
// 
//      // Initiate a delegate operation
//      function sendToSelf () {
//        return client.getActiveAccount().then((activeAccount) => {
//          return client.requestOperation({
//            operationDetails: [
//              {
//                kind: beacon.TezosOperationType.TRANSACTION,
//                destination: activeAccount?.address ?? '',
//                amount: '1'
//              }
//            ]
//          })
//        })
//      }
