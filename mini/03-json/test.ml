open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

let (!$) = Js.string

(* Js.to_string is also used often *)
let (?$) = Js.to_string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

(* JSON parser and printer object *)
let json : < parse : 'a . Js.js_string Js.t -> 'a Js.meth;
             stringify : 'a. 'a -> Js.js_string Js.t Js.meth > Js.t
  = Js.Unsafe.pure_js_expr "JSON"

(* The node.  It must be CORS enabled. See:
   https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node = "https://mainnet.smartpy.io"

let url =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9/balance"

(* A function to print mutez in tz xxx.yyy *)
let string_of_mutez_in_tz mtz =
  let tz, mtz' = Z.div_rem mtz (Z.of_int 1_000_000) in
  Printf.sprintf "tz %s.%06d" (Z.to_string tz) (Z.to_int mtz')

let start () =
  (* The node must be CORS enabled. *)
  let+ http_frame = XmlHttpRequest.perform url in
  match http_frame.code with
  | 200 ->
      (* the content has the form "\"12345\"".
         JSON parser parses it to a js_string "12345" *)
      let s : Js.js_string Js.t = json##parse (Js.string http_frame.content) in
      Console.log s;
      let balance = Z.of_string ?$s in
      let balance_json_string =
        (* Put it back to JSON string, using json##stringify *)
        json##stringify (Js.string @@ Z.to_string balance)
      in
      Console.logf "%s is %s in JSON"
        (string_of_mutez_in_tz balance)
        ?$balance_json_string
  | _ -> Console.logf "HTTP code %d" http_frame.code

module Html = Dom_html

let _ =
  Html.window##.onload := Html.handler (fun _ -> ignore @@ start (); Js._false)
