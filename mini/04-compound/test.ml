open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

let (!$) = Js.string

let (?$) = Js.to_string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

(* JSON parser and printer object *)
let json : < parse : 'a . Js.js_string Js.t -> 'a Js.meth;
             stringify : 'a. 'a -> Js.js_string Js.t Js.meth > Js.t
  = Js.Unsafe.pure_js_expr "JSON"

(* The node.  It must be CORS enabled. See:
   https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node = "https://mainnet.smartpy.io"

(* This time, we get more complex data of a contract *)
let url address =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ address

(* Type for the data we skip parsing this time (script) *)
type unknown

(* The data returned from the RPC is an JSON object, specified at
   https://tezos.gitlab.io/active/rpc.html#get-block-id-context-contracts-contract-id

   Here is the OCaml class type for the JS object isomorphic to it.
*)
class type contract = object

  (* balance always exists *)
  method balance : Js.js_string Js.t Js.readonly_prop

  (* this is an optional string field *)
  method delegate : Js.js_string Js.t Js.optdef Js.readonly_prop

  (* this field is optional, and has some complex structure if exists.
     we skip handling it for this time, using an abstract type [unknown].
     See 05-kt1/test.ml how to handle it (partially).
  *)
  method script : unknown Js.t Js.optdef Js.readonly_prop

  (* this is an optional string field *)
  method counter : Js.js_string Js.t Js.optdef Js.readonly_prop
end

let query_contract address =
  let+ http_frame = XmlHttpRequest.perform (url address) in
  match http_frame.code with
  | 200 ->
      (* Parse the string as an JSON of type contract *)
      let c : contract Js.t = json##parse (Js.string http_frame.content) in
      Console.logf "%s : balance: %s; delegate: %s; script: %s; counter: %s"
        address
        (?$ (c##.balance))
        (Js.Optdef.case c##.delegate (fun () -> "none") (?$))
        (Js.Optdef.case c##.script (fun () -> "none") (fun x ->
             (* In OCaml it is an abstract value but we can print it using json##stringify *)
             ?$ (json##stringify x)))
        (Js.Optdef.case c##.counter (fun () -> "none") (?$))

  | _ -> Console.logf "HTTP code %d" http_frame.code

module Html = Dom_html

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ query_contract "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9";
      Js._false)
