open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax
open Tezos_jsoo

let node = "https://mainnet.smartpy.io"

let url address =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ address

(* Let's add strings and HTML elements to the browser window *)
let append_js_string parent s =
  Dom.appendChild parent @@ document##createTextNode s
let append_string parent s = append_js_string parent !$s
let appendf parent fmt = Format.kasprintf (append_string parent) fmt
let append_node parent n =
  Dom.appendChild parent @@ document##createElement !$n
let append_json parent j = append_js_string parent (json##stringify j)

type unknown

class type script =
  let open Js in
  object
    method code : unknown t readonly_prop
    method storage : unknown t readonly_prop
  end

class type contract =
  let open Js in
  object
    method balance : js_string t readonly_prop
    method delegate : js_string t optdef readonly_prop
    method script : script t optdef readonly_prop
    method counter : js_string t optdef readonly_prop
  end

let query div address =
  let+ http_frame = XmlHttpRequest.perform (url address) in
  match http_frame.code with
  | 200 ->
      let c : contract Js.t = json##parse !$(http_frame.content) in
      appendf div "%s : balance: %s; delegate: %s; counter: %s"
        address
        (Js.to_string c##.balance)
        (Js.Optdef.case c##.delegate (fun () -> "none") (?$))
        (Js.Optdef.case c##.counter (fun () -> "none") (?$));
      append_node div "br";
      Js.Optdef.case c##.script
        (fun () -> append_string div "script: none")
        (fun script ->
           (* print the abstracted fields using json##stringify *)
           appendf div "code: %s" @@ (?$) @@ json##stringify script##.code;
           append_node div "br";
           appendf div "storage: %s" @@ (?$) @@ json##stringify script##.storage);
      append_node div "p"
  | _ ->
      appendf div "HTTP code %d" http_frame.code;
      append_node div "p"

module Html = Dom_html

let f (_sodium : Sodium.sodium Js.t) =
  (* find <div id="div"> *)
  let div = Html.getElementById "div" in
  (* find <input id="input"> *)
  let input = from_Some @@ Html.getElementById_coerce "input"
      Html.CoerceTo.input
  in
  (* when sodium is ready, add a button for query *)
  let button = document##createElement !$"button" in
  button##.innerText := !$"Query";
  Dom.appendChild div button;
  append_node div "p";
  button##.onclick :=
    Html.handler (fun _ev ->
        (* use the text in the input *)
        let n = input##.value in
        ignore @@ query div @@ (?$) n;
        Js._false)

(* sodium.js uses async load.
   We cannot use usual [Html.window##.onload := Html.handler]
   with libdsodium.js.  Instead, use [Sodium.set_onload]
*)
let _ = Tezos_jsoo.Sodium.with_sodium f
