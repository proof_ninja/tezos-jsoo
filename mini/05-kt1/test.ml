open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

let (!$) = Js.string

let (?$) = Js.to_string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

let json : < parse : 'a . Js.js_string Js.t -> 'a Js.meth;
             stringify : 'a. 'a -> Js.js_string Js.t Js.meth > Js.t
  = Js.Unsafe.pure_js_expr "JSON"

(* The node.  It must be CORS enabled. See:
   https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node = "https://mainnet.smartpy.io"

let url address =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/" ^ address

type unknown

(* script field of the contract is a record whose fields are
   code and storage.  They have even more complex structure
   called Micheline but this time we skip parsing them.
*)
class type script =
  let open Js in
  object
    method code : unknown t readonly_prop
    method storage : unknown t readonly_prop
  end

class type contract =
  let open Js in
  object
    method balance : js_string t readonly_prop
    method delegate : js_string t optdef readonly_prop
    method script : script t optdef readonly_prop
    method counter : js_string t optdef readonly_prop
  end

let query_contract address =
  let+ http_frame = XmlHttpRequest.perform (url address) in
  match http_frame.code with
  | 200 ->
      let c : contract Js.t = json##parse (Js.string http_frame.content) in
      Console.logf "%s : balance: %s; delegate: %s; counter: %s"
        address
        ((?$) c##.balance)
        (Js.Optdef.case c##.delegate (fun () -> "none") (?$))
        (Js.Optdef.case c##.counter (fun () -> "none") (?$));
      (* This time we have see the internal of script field *)
      Js.Optdef.case c##.script
        (fun () -> Console.log !$"script: none")
        (fun script ->
           (* print the abstracted fields using json##stringify *)
           Console.logf "code: %s" @@ (?$) @@ json##stringify script##.code;
           Console.logf "storage: %s" @@ (?$) @@ json##stringify script##.storage)
  | _ -> Console.logf "HTTP code %d" http_frame.code

module Html = Dom_html

let _ =
  Html.window##.onload := Html.handler (fun _ ->
      ignore @@ query_contract "tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9";
      ignore @@ query_contract "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N";
      Js._false)
