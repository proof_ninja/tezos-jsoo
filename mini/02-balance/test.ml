open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

let (!$) = Js.string

module Console = struct
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

(* The node.  It must be CORS enabled. See:
   https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node = "https://mainnet.smartpy.io"

(* Tezos RPC path to get the balance of an address *)
let url =
  Option.get
  @@ Url.url_of_string
  @@ node ^ "/chains/main/blocks/head/context/contracts/tz3RDC3Jdn4j15J7bBHZd29EUee9gVB1CxD9/balance"

let start () =
  (* The node must be CORS enabled.
     See https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
  *)
  let+ http_frame = XmlHttpRequest.perform url in
  match http_frame.code with
  | 200 ->
      Console.log !$(Printf.sprintf "%S" http_frame.content);
      (* It is like "\"12345\"\n".
         Let's removes double quotes and newline to obtain "12345"

         In 03-json/test.ml, we do this more nicely using the JSON parser.
      *)
      let n =
        let len = String.length http_frame.content in
        String.sub http_frame.content 1 (len-3)
      in
      (* balance in Tezos is an arbitrary-precision integer, in mutez (micro tez)

         To use module Z of Zarith library, we use zarith_stubs_js package to provide
         JS code for Z.t.  See dune file.
      *)
      let balance = Z.of_string n in
      (* let's print the balance in tz *)
      let tz, mtz = Z.div_rem balance (Z.of_int 1_000_000) in
      Console.logf "tz %s.%06d" (Z.to_string tz) (Z.to_int mtz)
  | _ -> Console.logf "HTTP code %d" http_frame.code

module Html = Dom_html

let _ =
  Html.window##.onload := Html.handler (fun _ -> ignore @@ start (); Js._false)
