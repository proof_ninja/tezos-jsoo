open Js_of_ocaml
open Js_of_ocaml_lwt
open Lwt.Syntax

(* We use plenty of string conversions from OCaml to JS.  A prefix op is handy. *)
let (!$) = Js.string

module Console = struct
  (* We often print things in the console. *)
  let log a = (Js.Unsafe.js_expr "console.log" : 'a -> unit) a

  (* Logging with fprintf interface.  Handy. *)
  let logf fmt = Format.kasprintf (fun s -> log !$s) fmt
end

(* The node.  It must be CORS enabled. See:
   https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node = "https://mainnet.smartpy.io"

(* Tezos RPC path to get the latest block info *)
let url = Option.get @@ Url.url_of_string (node ^ "/chains/main/blocks/head")

let start () =
  (* Node access via XmlHttpRequest.

     The node must be CORS enabled. See:
     https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
  *)
  let+ http_frame = XmlHttpRequest.perform url in (* let+ = monadmic map *)
  match http_frame.code with
  | 200 ->
      (* the response is usually very large *)
      Console.log !$(String.sub http_frame.content 0 1000)
  | _ -> Console.logf "HTTP code %d" http_frame.code

module Html = Dom_html

let _ =
  (* Call [start] when the page is loaded *)
  Html.window##.onload :=
    Html.handler (fun _ -> ignore @@ start (); Js._false)
