open Js_of_ocaml
(*
open Js_of_ocaml_lwt
open Lwt.Syntax
*)
open Tezos_jsoo

module Html = Dom_html

let f (_sodium : Sodium.t Js.t) =
  let client = Beacon.new_DAppClient ~name: !$"TezosJSOO" () in
  Console.log client;
  (* Beacon uses JS promises.  We use promise_jsoo *)
  let open Promise.Syntax in

  Promise.catch ~rejected:(fun err -> Console.log err; Promise.return ())
  @@
  let* res =
    Beacon.requestPermissions client
      ?network:None
      ?scopes:None
  in
  Console.log res;
  let* ainfo = Beacon.getActiveAccount client in
  Console.log ainfo;
  let* opresp = Beacon.requestOperation client
      [| Beacon.partialTezosTransactionOperation
           ~mutez: 1L
           ~destination:ainfo##.address ()
      |]
  in
  Console.log opresp;
  Promise.return ()

let _ = Sodium.with_sodium f
