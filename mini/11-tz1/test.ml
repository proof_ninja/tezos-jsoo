open Js_of_ocaml
open Tezos_jsoo

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

(* sodium library functions are given in the sodium JS object *)
let f (sodium : Sodium.t Js.t) =
  Console.log sodium;
  let secret_seed = Base58check.decode !$secret_seed_b58c 4 in
  (* ED25519 keypair is generated from a seed.
     keypair is a JS object with publicKey and privateKey fields.
  *)
  let keypair = sodium##crypto_sign_seed_keypair_ secret_seed in
  Console.log keypair;
  let public_key = keypair##.publicKey in
  (* Tezos public key is Base58Check-encoded with prefix "0d0f25d9" *)
  let public_key_b58c = Base58check.encode public_key !$"0d0f25d9" in
  Console.logf "public_key: %s" ?$public_key_b58c;
  (* tz1.. is created from 20 byte Blake2b hash of the public key. *)
  let pkhash = sodium##crypto_generichash_ 20 public_key in
  (* Base58Check prefix of Tezos ED25519 public key hashes is "06a19f" *)
  let pkhash_b58c = Base58check.encode pkhash !$"06a19f" in
  Console.logf "public_key_hash: %s" ?$pkhash_b58c

(* sodium.js uses async load.
   We cannot use usual [Html.window##.onload := Html.handler] with libdsodium.js.
   Instead, use [Sodium.set_onload]
*)
let _ = Tezos_jsoo.Sodium.with_sodium f
