open Js_of_ocaml

(* We use Tezos_jsoo library *)
open Tezos_jsoo

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

(* Base58Check de/encoders.  It calls functions defined in js/utils.js,
   which use js/base58check.js and js/sodium.js
*)
module Base58Check = struct
  open Js
  type uint8array

  let decode s prefixbytes =
    (Js.Unsafe.js_expr "b58c_decode" : js_string t -> int -> uint8array t )
      s prefixbytes

  let encode bin prefix =
    (Js.Unsafe.js_expr "b58c_encode" : uint8array t -> js_string t -> js_string t)
      bin prefix
end

let f _sodium (* It takes a sodium object but unused in this example *) =
  (* ED25519 seed is Base58Check encoded with 4 bytes prefix 0d0f3a07.
     The prefix is defined in src/lib_crypto/base58.ml of Tezos source code:

       [let ed25519_seed = "\013\015\058\007" (* edsk(54) *)]

     Some useful prefixes:

       Ed25519 public_key edpk.. : "0d0f25d9"
       Ed25519 signature edsig.. : "09f5cd8612"
  *)
  (* Once decoded, the 4 byte prefix must be removed *)
  let secret_seed = Base58check.decode (Js.string secret_seed_b58c) 4 in
  (* Encode back with the prefix *)
  let secret_seed_b58c' = Base58check.encode secret_seed !$"0d0f3a07" in
  Console.log secret_seed_b58c';
  assert (secret_seed_b58c = Js.to_string secret_seed_b58c')

(* sodium.js uses async load.
   We cannot use usual [Html.window##.onload := Html.handler]
   with libdsodium.js.

   Instead, use [Tezos_jsoo.Sodium.with_sodium]
*)
let _ = Tezos_jsoo.Sodium.with_sodium f
