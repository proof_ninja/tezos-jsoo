open Js_of_ocaml
open Tezos_jsoo

(* Unencrypted secret key found in .tezos-client/secret_keys.
   It is actually not a secret key but a 32 byte Ed25519 seed.

   Do not waste your time to steal tokens from Tezos mainnet using this key.
   It is empty.
*)
let secret_seed_b58c = "edsk47uGKzMePUwQQpdJuaiyow41H1S92RwSu9L3idSpHu9Gc5UMpd"

let f (sodium : Sodium.sodium Js.t) =
  let secret_seed = Base58check.decode (!$ secret_seed_b58c) 4 in
  (* ED25519 keypair is generated from a seed.
     keypair is a JS object with publicKey and privateKey fields.
  *)
  let keypair = sodium##crypto_sign_seed_keypair_ secret_seed in
  Console.log keypair;
  let secret_key = keypair##.privateKey in
  let public_key = keypair##.publicKey in
  (* message to sign *)
  let mes = Uint8array.encode !$"hello" in
  Console.log mes;
  (* sign with the secret key *)
  let sign = sodium##crypto_sign_detached_ ~mes ~sk:secret_key in
  (* Tezos Base58check encoded signature *)
  Console.logf "signature: %s" @@ (?$) @@ Base58check.encode sign !$"09f5cd8612";
  (* Check the signature with the public key *)
  assert (Js.to_bool @@ sodium##crypto_sign_verify_detached_ ~sign ~mes ~pk:public_key);
  Console.logf "signature successfully checked"

module Html = Dom_html

(* sodium.js uses async load.
   We cannot use usual [Html.window##.onload := Html.handler] with libdsodium.js.
   Instead, use [Sodium.set_onload]
*)
let _ = Tezos_jsoo.Sodium.with_sodium f
