open Js_of_ocaml
open Tezos_jsoo
open Tezos_jsoo.Infix

module Html = Dom_html

let document = Html.window##.document

(* The node must be CORS enabled.
   See https://tezos.stackexchange.com/questions/274/how-to-make-a-tezos-node-set-cors-headers
*)
let node  = Node.make "https://mainnet.smartpy.io"

let rec remove_children n =
  Js.Opt.case n##.firstChild
    (fun () -> ())
    (fun c ->
       Dom.removeChild n c;
       remove_children n)

let query () =
  let input =
    Js.Opt.get (Html.CoerceTo.input (getById "input")) (fun () -> assert false)
  in
  let n = input##.value in
  let address = Address (Js.to_string n) in
  let div = getById "div" in
  remove_children div;
  append_js_string div n;
  appendp div;

  RPC.balance node address
  >>=? fun balance ->
  appendf div "balance: %s" (Mutez.to_string balance);
  appendp div;

  RPC.counter node address
  >>=? fun counter ->
  appendf div "counter: %s" (Z.to_string counter);
  appendp div;

  RPC.contract' node address
  >>=? fun contract ->
  append_json div contract;
  appendp div;

  Lwt.return (Ok ())

let start _sodium =
  let res = Ed25519.Seed.decode
      (Js.string "edsk2mrJTWxHFipkwcSEwytwns13YJLdU7qUGk9Js8XfpgGkGgdvN3")
  in
  let res = Ed25519.Seed.encode res in
  Console.log res;

  let button = Js.Opt.get (Html.CoerceTo.button (getById "button")) (fun () -> assert false) in
  button##.onclick :=
    Html.handler (fun _ev ->
        ignore @@ query ();
        Js._false)

let _ = Sodium.with_sodium start
